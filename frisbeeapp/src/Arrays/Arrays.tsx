// Arrays for data. Normally this data would be in the database and handled trough mutates and queries, but I dont have backend or database, so I have to hardcoded arrays.
// These arrays can and are used. But everytime app is rerendered, data resets to these values.

export let fairwayAmount = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
];

export var courses = [
    {
        name: "Meri-Toppila DiscGolfPark",
        pars: [3, 3, 3, 3, 3, 3, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3],
        length: [
            109, 89, 110, 60, 71, 125, 86, 129, 160, 88, 85, 92, 90, 72, 93,
            114, 83, 108,
        ],
    },
    {
        name: "Hiironen",
        pars: [3, 3, 3, 5, 3, 3, 4, 3, 3, 3, 3, 3, 4, 3, 3, 3, 3, 3],
        length: [
            90, 74, 99, 243, 79, 80, 197, 121, 133, 123, 69, 77, 113, 98, 113,
            99, 87, 104,
        ],
    },
    {
        name: "Virpiniemi DiscGolfPark",
        pars: [3, 3, 3, 3, 3, 5, 4, 3, 4, 3, 5, 3, 4, 4, 5, 3, 3, 3],
        length: [
            70, 110, 111, 88, 78, 211, 109, 95, 167, 74, 256, 99, 206, 150, 226,
            116, 138, 96,
        ],
    },
];

export var rounds = [
    {
        course: "Meri-Toppila DiscGolfPark",
        date: "21.08.2021",
        results: [3, 2, 2, 3, 3, 3, 3, 3, 4, 2, 2, 4, 3, 3, 3, 3, 4, 3],
    },
];
