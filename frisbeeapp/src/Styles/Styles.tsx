import React from "react";

export const mainDivStyle = {
    width: "100vw",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
} as React.CSSProperties;

export const topDivStyle = {
    width: "100vw",
    height: "20vh",
    display: "flex",
    alignItems: "center",
} as React.CSSProperties;

export const backButtonDivStyle = {
    position: "absolute",
    left: "4vh",
} as React.CSSProperties;

export const titleDivStyle = {
    fontSize: "2rem",
    marginRight: "auto",
    marginLeft: "auto",
} as React.CSSProperties;

export const contentDivStyle = {
    width: "100vw",
    height: "80vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    overflow: "auto",
} as React.CSSProperties;

export const innerContentDivStyle = {
    width: "96vw",
    height: "78vh",
    display: "flex",
    flexDirection: "column",
} as React.CSSProperties;

export const textfieldDivStyle = {
    width: "40vw",
    marginLeft: "6vw",
};

export const dateDivStyle = {
    marginLeft: "6vw",
    marginTop: "2vh",
};

export const courseParametersDivStyle = {
    marginTop: "4vh",
    width: "96vw",
    height: "25vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
} as React.CSSProperties;

export const parameterDivStyle = {
    display: "flex",
    justifyContent: "space-between",
};

export const parameterTitleStyle = {
    width: "5vw",
    fontSize: "1.3rem",
    marginTop: "auto",
    marginBottom: "auto",
};

export const parDivStyle = {
    width: "4vw",
};

export const fairwayDivStyle = {
    width: "4vw",
    display: "flex",
    justifyContent: "center",
};

export const acceptButtonDivStyle = {
    width: "96vw",
    marginTop: "4vh",
};

export const innerCardDivStyle = {
    margin: "0.5vw",
};

export const cardStyle = { marginBottom: "1vh", minHeight: "27vh" };

export const dateAndCourseNameDivStyle = {
    display: "flex",
    fontSize: "1rem",
    marginBottom: "2vh",
};

export const textDivStyle = {
    marginRight: "2vw",
};

export const smallerCourseParametersDivStyle = {
    width: "95vw",
    height: "13vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
} as React.CSSProperties;

export const smallerParameterTitleStyle = {
    width: "5vw",
    fontSize: "1rem",
    marginTop: "auto",
    marginBottom: "auto",
};

export const resultParametersDivStyle = {
    marginTop: "2vh",
    width: "95vw",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
} as React.CSSProperties;

export const mainInMainViewDivStyle = {
    width: "100vw",
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly",
    alignItems: "center",
} as React.CSSProperties;

export const titleInMainViewDivStyle = {
    fontSize: "2rem",
};

export const buttonDivStyle = {
    width: "100%",
    height: "10%",
    display: "flex",
    justifyContent: "space-evenly",
};

export const mainButtonStyle = {
    width: "20%",
    height: "100%",
};
