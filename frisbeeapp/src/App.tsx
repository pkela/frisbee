import React from "react";

import SelectionView from "./Views/SelectionView";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    //@ts-ignore
} from "react-router-dom";

// App uses router, now only one route is available, but normally every view would be its own route

function App() {
    return (
        <Router>
            <Switch>
                <Route path="/">
                    <SelectionView />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
