import React from "react";

import {
    buttonDivStyle,
    mainButtonStyle,
    mainInMainViewDivStyle,
    titleInMainViewDivStyle,
} from "../Styles/Styles";

import Button from "@material-ui/core/Button";

export type mainProps = {
    setView: (view: string) => void;
};

export default function MainView(props: mainProps) {
    return (
        <div style={mainInMainViewDivStyle}>
            <div style={titleInMainViewDivStyle}>
                Tervetuloa frisbeegolf sovellukseen!
            </div>
            <div style={buttonDivStyle}>
                <Button
                    variant="outlined"
                    color="primary"
                    style={mainButtonStyle}
                    onClick={() => {
                        props.setView("newfield");
                    }}
                >
                    {"Lisää uusi kenttä"}
                </Button>
                <Button
                    variant="outlined"
                    color="primary"
                    style={mainButtonStyle}
                    onClick={() => {
                        props.setView("newround");
                    }}
                >
                    {"Lisää uusi kierros"}
                </Button>
                <Button
                    variant="outlined"
                    color="primary"
                    style={mainButtonStyle}
                    onClick={() => {
                        props.setView("playedrounds");
                    }}
                >
                    {"Pelatut kierrokset"}
                </Button>
            </div>
        </div>
    );
}
