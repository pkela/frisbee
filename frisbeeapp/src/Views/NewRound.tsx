import React, { useState } from "react";

import {
    mainDivStyle,
    topDivStyle,
    backButtonDivStyle,
    titleDivStyle,
    contentDivStyle,
    innerContentDivStyle,
    textfieldDivStyle,
    dateDivStyle,
    courseParametersDivStyle,
    parameterDivStyle,
    parameterTitleStyle,
    parDivStyle,
    fairwayDivStyle,
    acceptButtonDivStyle,
} from "../Styles/Styles";

import { fairwayAmount } from "../Arrays/Arrays";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

import { courseType, roundType } from "../TypeDefinitions/typeDefs";

export type newRoundProps = {
    setView: (view: string) => void;
    allCourses: courseType[];
    allRounds: roundType[];
    setAllRounds: (round: roundType[]) => void;
};

export default function NewRound(props: newRoundProps) {
    const [selectedCourse, setSelectedCourse] = useState<courseType | null>();
    const [date, setDate] = useState<string>("");
    const [parArray, setParArray] = useState<number[]>(fairwayAmount);

    // Functions to map fairway numbers, par boxes and length boxes

    const mapFairwayNumbers = (fairway: number, index: number) => {
        return (
            <div key={index} style={fairwayDivStyle}>
                {fairway}
            </div>
        );
    };

    const mapParBoxes = (fairway: number, index: number) => {
        return (
            <div key={index} style={parDivStyle}>
                <TextField
                    variant="outlined"
                    value={parArray[index] || ""}
                    onChange={(event) => setNewPar(event.target.value, index)}
                />
            </div>
        );
    };

    const mapLengthBoxes = (fairway: number, index: number) => {
        return (
            <div key={index} style={parDivStyle}>
                <TextField
                    variant="outlined"
                    value={selectedCourse?.length[index] || ""}
                    disabled
                />
            </div>
        );
    };

    // Functions to set new values to pars as result and to add new round
    // If I had database connected, setNewRound would send the new round to database

    const setNewPar = (value: string, index: number) => {
        var temporaryArray = [...parArray];

        temporaryArray[index] = parseInt(value);

        setParArray(temporaryArray);
    };

    const setNewRound = () => {
        var temporaryArray = [...props.allRounds];

        temporaryArray.push({
            course: selectedCourse ? selectedCourse.name : "",
            date: date,
            results: parArray,
        });

        props.setAllRounds(temporaryArray);
        props.setView("");
    };

    return (
        <div style={mainDivStyle}>
            <div style={topDivStyle}>
                <div style={backButtonDivStyle}>
                    <Button
                        color="primary"
                        onClick={() => {
                            props.setView("");
                        }}
                    >
                        {"< Takaisin"}
                    </Button>
                </div>
                <div style={titleDivStyle}>Lisää uusi kierros</div>
            </div>
            <div style={contentDivStyle}>
                <div style={innerContentDivStyle}>
                    <div style={textfieldDivStyle}>
                        <Autocomplete
                            options={props.allCourses}
                            getOptionLabel={(option) => option.name}
                            onChange={(event, value) => {
                                console.log(value);
                                setSelectedCourse(value);
                            }}
                            fullWidth
                            renderInput={(params) => (
                                <TextField
                                    {...params}
                                    placeholder={"Valitse kenttä"}
                                    variant="outlined"
                                />
                            )}
                        />
                    </div>
                    <div style={dateDivStyle}>
                        <TextField
                            value={date}
                            onChange={(event) => setDate(event.target.value)}
                            placeholder={"Päivämäärä"}
                            variant="outlined"
                        />
                    </div>
                    <div style={courseParametersDivStyle}>
                        <div style={parameterDivStyle}>
                            <div style={parameterTitleStyle}></div>
                            {fairwayAmount.map(
                                (fairway: number, index: number) =>
                                    mapFairwayNumbers(fairway, index)
                            )}
                        </div>
                        <div style={parameterDivStyle}>
                            <div style={parameterTitleStyle}>Par:</div>
                            {fairwayAmount.map(
                                (fairway: number, index: number) =>
                                    mapParBoxes(fairway, index)
                            )}
                        </div>
                        <div style={parameterDivStyle}>
                            <div style={parameterTitleStyle}>Pituus:</div>
                            {fairwayAmount.map(
                                (fairway: number, index: number) =>
                                    mapLengthBoxes(fairway, index)
                            )}
                        </div>
                    </div>
                    <div style={acceptButtonDivStyle}>
                        <Button
                            color="primary"
                            variant="outlined"
                            onClick={() => {
                                setNewRound();
                            }}
                        >
                            {"Lisää"}
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
}
