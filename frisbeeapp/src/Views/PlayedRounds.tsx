import React from "react";

import {
    mainDivStyle,
    topDivStyle,
    backButtonDivStyle,
    titleDivStyle,
    contentDivStyle,
    innerContentDivStyle,
    parameterDivStyle,
    parDivStyle,
    fairwayDivStyle,
    resultParametersDivStyle,
    innerCardDivStyle,
    smallerCourseParametersDivStyle,
    smallerParameterTitleStyle,
    textDivStyle,
    dateAndCourseNameDivStyle,
    cardStyle,
} from "../Styles/Styles";

import { fairwayAmount } from "../Arrays/Arrays";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";

import { courseType, roundType } from "../TypeDefinitions/typeDefs";

export type playedRoundsProps = {
    setView: (view: string) => void;
    allCourses: courseType[];
    allRounds: roundType[];
};

export default function PlayedRounds(props: playedRoundsProps) {
    // Functions to map fairway numbers, par boxes, length boxes and result boxes

    const mapFairwayNumbers = (fairway: number, index: number) => {
        return (
            <div key={index} style={fairwayDivStyle}>
                {fairway}
            </div>
        );
    };

    const mapParBoxes = (par: number, index: number) => {
        return (
            <div key={index} style={parDivStyle}>
                <TextField
                    size="small"
                    variant="outlined"
                    value={par}
                    disabled
                />
            </div>
        );
    };

    const mapLengthBoxes = (length: number, index: number) => {
        return (
            <div key={index} style={parDivStyle}>
                <TextField
                    size="small"
                    variant="outlined"
                    value={length}
                    disabled
                />
            </div>
        );
    };

    const mapResultBoxes = (result: number, index: number) => {
        return (
            <div key={index} style={parDivStyle}>
                <TextField
                    size="small"
                    variant="outlined"
                    value={result}
                    disabled
                />
            </div>
        );
    };

    // Function to map each result to own card

    const mapResultsToCards = (round: roundType, index: number) => {
        let course = props.allCourses.find(
            (course) => course.name === round.course
        );

        const reducer = (accumulator: number, current: number) =>
            accumulator + current;

        let courseParSum = course?.pars.reduce(reducer);
        let resultParSum = round.results.reduce(reducer);

        let resultTotal = resultParSum - (courseParSum ? courseParSum : 0);

        return (
            <Card key={index} style={cardStyle}>
                <div style={innerCardDivStyle}>
                    <div style={dateAndCourseNameDivStyle}>
                        <div style={textDivStyle}>{round.date}</div>
                        <div style={textDivStyle}>{round.course}</div>
                        <div style={textDivStyle}>
                            Tulos:{" "}
                            {resultTotal > 0 ? "+" + resultTotal : resultTotal}
                        </div>
                    </div>
                    <div style={smallerCourseParametersDivStyle}>
                        <div style={parameterDivStyle}>
                            <div style={smallerParameterTitleStyle}></div>
                            {fairwayAmount.map(
                                (fairway: number, index: number) =>
                                    mapFairwayNumbers(fairway, index)
                            )}
                        </div>
                        <div style={parameterDivStyle}>
                            <div style={smallerParameterTitleStyle}>Par:</div>
                            {course?.pars.map((par: number, index: number) =>
                                mapParBoxes(par, index)
                            )}
                        </div>
                        <div style={parameterDivStyle}>
                            <div style={smallerParameterTitleStyle}>
                                Pituus:
                            </div>
                            {course?.length.map(
                                (length: number, index: number) =>
                                    mapLengthBoxes(length, index)
                            )}
                        </div>
                    </div>
                    <div style={resultParametersDivStyle}>
                        <div style={parameterDivStyle}>
                            <div style={smallerParameterTitleStyle}>Tulos:</div>
                            {round.results.map(
                                (result: number, index: number) =>
                                    mapResultBoxes(result, index)
                            )}
                        </div>
                    </div>
                </div>
            </Card>
        );
    };

    return (
        <div style={mainDivStyle}>
            <div style={topDivStyle}>
                <div style={backButtonDivStyle}>
                    <Button
                        color="primary"
                        onClick={() => {
                            props.setView("");
                        }}
                    >
                        {"< Takaisin"}
                    </Button>
                </div>
                <div style={titleDivStyle}>Pelatut kierrokset</div>
            </div>
            <div style={contentDivStyle}>
                <div style={innerContentDivStyle}>
                    {props.allRounds.map((round, index) =>
                        mapResultsToCards(round, index)
                    )}
                </div>
            </div>
        </div>
    );
}
