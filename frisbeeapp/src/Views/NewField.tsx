import React, { useState } from "react";

import {
    mainDivStyle,
    topDivStyle,
    backButtonDivStyle,
    titleDivStyle,
    contentDivStyle,
    innerContentDivStyle,
    textfieldDivStyle,
    courseParametersDivStyle,
    parameterDivStyle,
    parameterTitleStyle,
    parDivStyle,
    fairwayDivStyle,
    acceptButtonDivStyle,
} from "../Styles/Styles";

import { fairwayAmount } from "../Arrays/Arrays";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import { courseType } from "../TypeDefinitions/typeDefs";

export type newFieldProps = {
    setView: (view: string) => void;
    allCourses: courseType[];
    setAllCourses: (courses: courseType[]) => void;
};

export default function NewField(props: newFieldProps) {
    const [courseName, setCourseName] = useState<string>("");
    const [parArray, setParArray] = useState<number[]>(fairwayAmount);
    const [lengthArray, setLengthArray] = useState<number[]>(fairwayAmount);

    // Functions to map par and length boxes for each fairway and fairway numbers

    const mapFairwayNumbers = (fairway: number, index: number) => {
        return (
            <div key={index} style={fairwayDivStyle}>
                {fairway}
            </div>
        );
    };

    const mapParBoxes = (fairway: number, index: number) => {
        return (
            <div key={index} style={parDivStyle}>
                <TextField
                    variant="outlined"
                    value={parArray[index] || ""}
                    onChange={(event) => setNewPar(event.target.value, index)}
                />
            </div>
        );
    };

    const mapLengthBoxes = (fairway: number, index: number) => {
        return (
            <div key={index} style={parDivStyle}>
                <TextField
                    variant="outlined"
                    value={lengthArray[index] || ""}
                    onChange={(event) =>
                        setNewLength(event.target.value, index)
                    }
                />
            </div>
        );
    };

    // Functions to set new values for par, length and set new course
    // If I had database, setAllCourses would send the new course to database

    const setNewPar = (value: string, index: number) => {
        var temporaryArray = [...parArray];

        temporaryArray[index] = parseInt(value);

        setParArray(temporaryArray);
    };

    const setNewLength = (value: string, index: number) => {
        var temporaryArray = [...lengthArray];

        temporaryArray[index] = parseInt(value);

        setLengthArray(temporaryArray);
    };

    const setAllCourses = () => {
        var temporaryArray = [...props.allCourses];

        temporaryArray.push({
            name: courseName,
            pars: parArray,
            length: lengthArray,
        });

        props.setAllCourses(temporaryArray);
        props.setView("");
    };

    return (
        <div style={mainDivStyle}>
            <div style={topDivStyle}>
                <div style={backButtonDivStyle}>
                    <Button
                        color="primary"
                        onClick={() => {
                            props.setView("");
                        }}
                    >
                        {"< Takaisin"}
                    </Button>
                </div>
                <div style={titleDivStyle}>Lisää uusi kenttä</div>
            </div>
            <div style={contentDivStyle}>
                <div style={innerContentDivStyle}>
                    <div style={textfieldDivStyle}>
                        <TextField
                            value={courseName}
                            onChange={(event) =>
                                setCourseName(event.target.value)
                            }
                            placeholder={"Kentän nimi"}
                            variant="outlined"
                            fullWidth
                        />
                    </div>
                    <div style={courseParametersDivStyle}>
                        <div style={parameterDivStyle}>
                            <div style={parameterTitleStyle}></div>
                            {fairwayAmount.map(
                                (fairway: number, index: number) =>
                                    mapFairwayNumbers(fairway, index)
                            )}
                        </div>
                        <div style={parameterDivStyle}>
                            <div style={parameterTitleStyle}>Par:</div>
                            {fairwayAmount.map(
                                (fairway: number, index: number) =>
                                    mapParBoxes(fairway, index)
                            )}
                        </div>
                        <div style={parameterDivStyle}>
                            <div style={parameterTitleStyle}>Pituus:</div>
                            {fairwayAmount.map(
                                (fairway: number, index: number) =>
                                    mapLengthBoxes(fairway, index)
                            )}
                        </div>
                    </div>
                    <div style={acceptButtonDivStyle}>
                        <Button
                            color="primary"
                            variant="outlined"
                            onClick={() => {
                                setAllCourses();
                            }}
                        >
                            {"Lisää"}
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
}
