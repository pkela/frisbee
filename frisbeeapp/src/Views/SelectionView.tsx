import React, { useState } from "react";

import MainView from "./MainView";
import NewField from "./NewField";
import NewRound from "./NewRound";
import PlayedRounds from "./PlayedRounds";

import { courses, rounds } from "../Arrays/Arrays";

export default function SelectionView() {
    const [view, setView] = useState<string>("");
    const [allCourses, setAllCourses] = useState(courses);
    const [allRounds, setAllRounds] = useState(rounds);

    // Function to look what view should be rendered. If i had database, I wouldn't need to use such method, but use Link instead, but
    // now when I dont have databse, I cant query data from there, so I have to store it in states and I cant use them, if I use Link nad router

    const viewRenderer = () => {
        switch (view) {
            case "":
                return <MainView setView={setView} />;
            case "newfield":
                return (
                    <NewField
                        setView={setView}
                        allCourses={allCourses}
                        setAllCourses={setAllCourses}
                    />
                );
            case "newround":
                return (
                    <NewRound
                        setView={setView}
                        allCourses={allCourses}
                        allRounds={allRounds}
                        setAllRounds={setAllRounds}
                    />
                );
            case "playedrounds":
                return (
                    <PlayedRounds
                        setView={setView}
                        allCourses={allCourses}
                        allRounds={allRounds}
                    />
                );
            default:
                return <MainView setView={setView} />;
        }
    };

    return viewRenderer();
}
