import React from "react";

export type courseType = {
    name: string;
    pars: number[];
    length: number[];
};

export type roundType = {
    course: string;
    date: string;
    results: number[];
};
